<!DOCTYPE SYSTEM>
<html>
    <head>
        <title>Calculator</title>
    </head>
    <body>
        <h2>Results Page: </h2>
        
        <?php
        
            $num1 = $_POST['number1'];
            $num2 = $_POST['number2'];
            $operation = $_POST['operation'];
  
            if(!is_numeric($num1) || !is_numeric($num2)){
                $result = "Please enter numbers!";
            }
            elseif ($operation  == "addition"){
                $result = $num1 + $num2;
            }
            elseif ($operation == "subtraction"){
                $result = $num1 - $num2;
            }
            elseif ($operation == "multiplication"){
                $result = $num1*$num2;
            }
            elseif ($operation == "division"){
                if ($num2 == 0){
                    $result = "Can't divide by zero!";    
                }
                else{
                    $result = $num1/$num2;
                }
            }
            else {
                $result = "Unhandled Error!";
            }
            
            echo "Result: " . $result;
        ?>
        
        <br/>
        <br/>
        
        <a href="/Module2/Calculator/calculator.php">Back To Calculator</a>
        
    </body>
</html>
